from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from . import datacenter, __version__, database

app = FastAPI(version=__version__, title="Veilthroch API", openapi_prefix="/api")
app.add_middleware(CORSMiddleware, allow_origins=["*"])
app.include_router(datacenter.router, prefix="/datacenter", tags=["datacenter"])


@app.on_event("startup")
async def startup() -> None:
    await datacenter.init()
