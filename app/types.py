from __future__ import annotations
import inspect
from typing import Any, Dict, Union

import bson.objectid
from bson.errors import InvalidId

from app import database


class ObjectId(str):
    @classmethod
    def get_validators(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value):
        try:
            bson.objectid.ObjectId(str(value))
        except InvalidId as e:
            raise ValueError(f"Invalid ObjectId", e)

        return cls(value)

    def __repr__(self):
        return f'ObjectId\'("{str(self)}")'

    def bson(self):
        return bson.objectid.ObjectId(str(self))


class StrictInt(int):
    @classmethod
    def get_validators(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value):
        try:
            res = int(value)
            if str(res) != str(value):
                raise ValueError("Is not a whole number !")

            return res

        except ValueError as e:
            raise ValueError(f"Invalid int", e)


class LinkMaker:
    def __init__(self, make_func):
        self.links = {}
        self.make_func = make_func

    def __getitem__(self, model):
        key = model.__name__ if hasattr(model, "__name__") else model

        if key not in self.links:
            self.links[key] = self.make_func(model)

        return self.links[key]


class BaseLink(str):
    __target_model__ = None

    @classmethod
    def get_validators(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value):
        str = ObjectId.validate(value)
        return cls(str)

    @property
    def id(self):
        return str(self)

    def __eq__(self, other: BaseLink) -> bool:
        return self.__target_model__ == other.__target_model__ and self.id == other.id

    def __hash__(self):
        return hash((self.__target_model__.__name__, self.id))

    def __repr__(self):
        return f"Link({self.__target_model__.__name__} {self.id})"

    def resolve(self):
        return self.__target_model__.find_one({"_id": self.id})

    def bson(self):
        return bson.objectid.ObjectId(self.id)


def makeLink(model):
    # if not inspect.isclass(model) or not issubclass(model, database.Model):
    #     raise ValueError(f"Link type has to be a Model class, not {type(model)}")

    class Link(BaseLink):
        __target_model__ = model

    return Link


class BaseDcLink(int):
    __target__ = None

    @classmethod
    def get_validators(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value):
        return cls(value)

    @classmethod
    def target_name(cls):
        target = cls.__target__

        if isinstance(target, str):
            return target

        if isinstance(target, type):
            return target.__name__

        raise ValueError(f"Wtf is target ? {repr(target)}")

    @property
    def id(self):
        return int(self)

    def __repr__(self):
        return f"DcLink({self.target_name()} {self.id})"

    def resolve(self, mapping: Dict[str, Dict[int, ObjectId]]):
        target = self.target_name()

        if target not in mapping:
            raise ValueError(f"Dc name '{target}' not yet loaded")

        if self.id not in mapping[target]:
            raise ValueError(f"Dc {target}/{self.id} not found")

        return mapping[target][self.id]


def makeDcLink(target):
    # if not isinstance(target, str):
    #     raise ValueError(f"Link type has to be a str class, not {type(target)}")

    class DcLink(BaseDcLink):
        __target__ = target

    return DcLink


Link = LinkMaker(makeLink)
DcUploadLink = LinkMaker(makeDcLink)


class ForeignKey:
    def __getitem__(cls, item):
        return Union[Link[item], item]


ForeignKey = ForeignKey()


class DcForeignKey:
    def __getitem__(cls, item):
        return Union[DcUploadLink[item], Link[item], item]


DcForeignKey = DcForeignKey()
