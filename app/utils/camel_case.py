import re
from typing import Union

from pydantic import BaseModel, BaseConfig


class CamelCaseSchema(BaseModel):
    """implements camel case outside, snake case inside"""

    def dict(
        self,
        *,
        include: Union["SetIntStr", "DictIntStrAny"] = None,
        exclude: Union["SetIntStr", "DictIntStrAny"] = None,
        by_alias: bool = True,
        skip_defaults: bool = False,
    ) -> "DictStrAny":
        """
        Generate a dictionary representation of the model, optionally specifying which fields to include or exclude.
        """
        return super().dict(
            include=include,
            exclude=exclude,
            by_alias=by_alias,
            skip_defaults=skip_defaults,
        )

    class Config(BaseConfig):
        @classmethod
        def alias_generator(cls, name):
            """turn _x into X, but only if not at begining of string or _ is before it"""
            return re.sub(r"(?:_)([a-z])", lambda m: m.group(1).upper(), name)

        allow_population_by_alias = True
