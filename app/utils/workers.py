import asyncio


def create(n, worker, args=tuple()):
    queue = asyncio.Queue()

    workers = []
    for i in range(n):
        task = asyncio.create_task(worker(queue, *args))
        workers.append(task)

    return queue, workers


async def stop(workers):
    for worker in workers:
        worker.cancel()
    await asyncio.gather(*workers, return_exceptions=True)
