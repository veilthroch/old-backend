from typing import Union, Optional, List, Tuple, Type, TypeVar, Any, Dict

import aenum
import pymongo
from motor.motor_asyncio import AsyncIOMotorClient
from pydantic import Schema, BaseModel
from pymongo import ReturnDocument
from pymongo.collation import Collation

from app.types import ObjectId
from app.utils import camel_case

database = AsyncIOMotorClient()["veilthroch"]

T = TypeVar("T")


def to_bson(obj: Any) -> Any:
    if hasattr(obj, "bson") and callable(obj.bson):
        return obj.bson()
    elif isinstance(obj, list):
        return [to_bson(item) for item in obj]
    elif isinstance(obj, dict):
        return {key: to_bson(value) for key, value in obj.items()}
    else:
        return obj


class Model(camel_case.CamelCaseSchema):
    """Represents MongoDB document"""

    pk: ObjectId = Schema(
        None,
        alias="_id",
        title="Database primary key",
        description="Must be MongoDB ObjectId.",
    )

    def primary_key(self) -> Dict[str, Any]:
        return self.bson(include={"pk"}) if self.pk else {}

    async def save(self: T, by_natural_key=True) -> T:
        """Replaces or Inserts current object to database by primary key"""
        filter = self.primary_key()

        if not filter and by_natural_key and hasattr(self, "natural_key"):
            filter = self.natural_key()

        result = await self.find_one_and_replace(filter, self, upsert=True)
        r = self.from_mongo(result)
        self.pk = r.pk

        return r

    def bson(
        self,
        *,
        include: Union["SetIntStr", "DictIntStrAny"] = None,
        exclude: Union["SetIntStr", "DictIntStrAny"] = None,
        by_alias: bool = True,
        skip_defaults: bool = False,
    ) -> Dict[str, Any]:
        return to_bson(
            self.dict(
                include=include,
                exclude=exclude,
                by_alias=by_alias,
                skip_defaults=skip_defaults,
            )
        )

    @classmethod
    def from_mongo(cls: Type[T], mongo: Dict[str, Any]) -> T:
        return cls.parse_obj(mongo)

    @classmethod
    async def find_one(
        cls: Type[T], filter: Dict[str, Any] = None, *args, **kwargs
    ) -> Optional[T]:
        result = await cls.collection().find_one(filter, *args, **kwargs)
        if result is None:
            return None

        return cls.from_mongo(result)

    @classmethod
    async def find(
        cls: Type[T], filter: Dict[str, Any] = None, *args, **kwargs
    ) -> Optional[T]:
        result = await cls.collection().find(filter, *args, **kwargs).to_list(None)

        if result is None:
            return []

        return [cls.from_mongo(x) for x in result]

    @classmethod
    async def find_one_and_replace(
        cls: Type[T],
        filter: Dict[str, Any],
        replacement: Any,
        return_document=ReturnDocument.AFTER,
        **kwargs,
    ) -> Optional[T]:
        if isinstance(replacement, Model):
            replacement = replacement.bson(exclude={"pk"}, by_alias=False)

        result = await cls.collection().find_one_and_replace(
            filter, replacement, return_document=return_document, **kwargs
        )
        if result is None:
            return None

        return cls.from_mongo(result)

    @classmethod
    async def init(cls):
        await cls.create_indices()

    @classmethod
    async def create_indices(cls):
        indices = cls.indices()
        if indices is None:
            return

        for index in indices:
            fields = [(name, type.value) for name, type in index.data]
            kwargs = index.dict(exclude={"data"}, by_alias=True, skip_defaults=True)

            await cls.collection().create_index(fields, **kwargs)

    class Database:
        collection = None
        indices = None

    @classmethod
    def _database_attribute(cls, name, nullable=False):
        if not hasattr(cls, "Database"):
            raise ValueError(f"DB model must have Database class!")

        if not hasattr(cls.Database, name):
            if nullable:
                return None

            raise ValueError(f"DB model must have Database.{name} attribute!")

        return getattr(cls.Database, name)

    @classmethod
    def collection(cls):
        """Database collection this model is stored in"""
        return cls._database_attribute("collection")

    @classmethod
    def indices(cls) -> List["Index"]:
        """Definition of indices on this model"""
        return cls._database_attribute("indices", nullable=True)


class EmbeddedModel(camel_case.CamelCaseSchema):
    """Represents embedded database document"""

    pass


class Index(BaseModel):
    """Used for specifying index creation rules"""

    class Type(aenum.Enum):
        """Type of index"""

        ASCENDING = pymongo.ASCENDING
        DESCENDING = pymongo.DESCENDING
        HASHED = pymongo.HASHED
        TEXT = pymongo.TEXT

    data: List[Tuple[str, Type]]
    name: Optional[str] = None

    unique: bool = False
    background: bool = False
    sparse: bool = False
    expire_after_seconds: Optional[int] = Schema(None, alias="expireAfterSeconds")
    collation: Optional[Collation] = None

    class Config:
        arbitrary_types_allowed = True
