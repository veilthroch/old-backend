from collections import defaultdict
from typing import List, Dict, Type

import bson

from app.database import Model, ObjectId
from app.types import BaseLink


class DbResolver:
    def __init__(self, data, known=None):
        self.data = data
        self.known = {}
        self.classes: Dict[str, Type[Model]] = {}

        self._extract_known(data)
        if known is not None:
            self._extract_known(known)

    def _extract_known(self, known):
        if isinstance(known, Model) and known.pk is not None:
            self.known[known.__class__.__name__, known.pk] = known
            self._register_class(known.__class__)

            return

        if isinstance(known, list):
            for obj in known:
                self._extract_known(obj)
            return

        raise ValueError("Unknown known !")

    def _group_deps(self, links: List[BaseLink]) -> Dict[str, List[ObjectId]]:
        result: Dict[str, List[ObjectId]] = defaultdict(list)

        for link in links:
            self._register_class(link.__target_model__)
            result[link.__target_model__.__name__] += [bson.objectid.ObjectId(link.id)]

        return result

    def _get_deps(self):
        if isinstance(self.data, list):
            links = (self._find_links(obj.dict()) for obj in self.data)
            return list({link for sub in links for link in sub})

        return list(set(self._find_links(self.data.dict())))

    async def _fetch_deps(self):
        deps = self._get_deps()
        groupped = self._group_deps(deps)

        for cls_name, ids in groupped.items():
            cls = self.classes[cls_name]
            resolved = await cls.find({"_id": {"$in": list(ids)}})
            self._extract_known(resolved)

    async def resolve(self):
        self.data = self._resolve_known(self.data)
        await self._resolve_round()

    async def _resolve_round(self):
        await self._fetch_deps()
        self.data = self._resolve_known(self.data)

    def _resolve_known(self, data):
        if isinstance(data, Model):
            dict = self._resolve_known_links(data.dict())
            return data.__class__(**dict)

        if isinstance(data, list):
            return [self._resolve_known(obj) for obj in data]

    def _resolve_known_links(self, obj):
        if isinstance(obj, BaseLink):
            key = obj.__target_model__.__name__, obj.id
            resolved = (
                self._resolve_known_links(self.known[key]) if key in self.known else obj
            )
            return resolved

        elif isinstance(obj, list):
            return [self._resolve_known_links(item) for item in obj]
        elif isinstance(obj, dict):
            return {key: self._resolve_known_links(value) for key, value in obj.items()}
        else:
            return obj

    @classmethod
    def _find_links(cls, obj):
        if isinstance(obj, BaseLink):
            yield obj
        elif isinstance(obj, list):
            for item in obj:
                yield from cls._find_links(item)
        elif isinstance(obj, dict):
            for key, value in obj.items():
                yield from cls._find_links(value)

    def _register_class(self, cls: Type[Model]):
        self.classes[cls.__name__] = cls
