from typing import Dict, Any

from fastapi import Path, Depends
from pydantic import Schema

from app.database.database import Model, Index
from . import enums


class DatacenterKey:
    """Natural key of any datacenter: region and version"""

    def __init__(
        self,
        region: enums.Region = Path(
            ..., description="Region language code from which this object is"
        ),
        patch: int = Path(..., description="Datacenter version number"),
    ):
        self.region = region
        self.patch = patch


class DatacenterModelKey:
    """Natural key of any datacenter top level object: it's id, and datacenter region/version"""

    def __init__(
        self,
        id: int = Path(..., description="Datacenter id of object"),
        datacenter: DatacenterKey = Depends(),
    ):
        self.id = id
        self.region = datacenter.region
        self.patch = datacenter.patch


class DatacenterModel(Model):
    """Represents a model in specific datacenter"""

    id: int = Schema(..., description="Datacenter id of object")
    region: enums.Region = Schema(
        ..., description="Region language code from which this object is"
    )
    patch: int = Schema(..., description="Datacenter version number")

    def natural_key(self) -> Dict[str, Any]:
        return self.bson(include={"id", "region", "patch"})

    class Database:
        indices = [
            Index(
                data=[
                    ("id", Index.Type.ASCENDING),
                    ("region", Index.Type.ASCENDING),
                    ("patch", Index.Type.ASCENDING),
                ],
                unique=True,
                name="natural_key",
            )
        ]
