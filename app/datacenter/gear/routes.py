from fastapi import APIRouter, HTTPException, Depends

from . import models
from app.datacenter.base_defs import DatacenterModelKey

router = APIRouter()


@router.get("/{id}", response_model=models.Enchant)
async def get_passivity(key: DatacenterModelKey = Depends()):
    """Gets passivity from datacenter database"""
    enchant = await models.Enchant.find_one(key.__dict__)

    if enchant is None:
        raise HTTPException(status_code=404, detail="Enchant was not found")

    return enchant
