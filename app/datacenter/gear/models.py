from typing import List, Union, Optional

from pydantic import Schema

import app.datacenter.base_defs as base

from app.database.database import database, EmbeddedModel
from app.datacenter import enums
from app.datacenter.passivity.models import PassivityCategory
from app.types import Link


class EnchantStat(EmbeddedModel):
    step: Optional[int] = Schema(None)
    rate: float
    stat: enums.BasicStat


class EnchantEffect(EmbeddedModel):
    step: Optional[int] = Schema(None)
    passivity_category: Union[Link[PassivityCategory], PassivityCategory] = Schema(...)


class Enchant(base.DatacenterModel):
    stats: List[EnchantStat] = Schema([])
    effects: List[EnchantEffect] = Schema([])

    class Database(base.DatacenterModel.Database):
        collection = database.datacenter.enchant


class EquipmentData(base.DatacenterModel):
    level: Optional[int] = Schema(None)
    type: Optional[enums.EquipmentType] = Schema(None)
    part: Optional[enums.EquipmentPart] = Schema(None)

    attack_min: Optional[int] = Schema(None)
    attack_max: Optional[int] = Schema(None)
    physical_attack_min: Optional[int] = Schema(None)
    physical_attack_max: Optional[int] = Schema(None)
    magical_attack_min: Optional[int] = Schema(None)
    magical_attack_max: Optional[int] = Schema(None)
    impact: Optional[int] = Schema(None)

    defense: Optional[int] = Schema(None)
    physical_defense: Optional[int] = Schema(None)
    magical_defense: Optional[int] = Schema(None)
    balance: Optional[int] = Schema(None)

    crystal_count: Optional[int] = Schema(None)
    lock: bool = Schema(False)

    class Database:
        collection = database.datacenter.equipment


async def init() -> None:
    await Enchant.init()
    await EquipmentData.init()
