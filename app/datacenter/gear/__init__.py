from . import models
from .routes import router


async def init() -> None:
    await models.init()
