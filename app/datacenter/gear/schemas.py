from typing import List, Optional

from pydantic import Schema

from app.datacenter.passivity.models import PassivityCategory
from app.types import DcUploadLink
from . import models


class EnchantEffect(models.EnchantEffect):
    passivity_category: DcUploadLink[PassivityCategory]


class Enchant(models.Enchant):
    effects: List[EnchantEffect] = Schema([])

    @property
    def converts_to_class(self):
        return models.Enchant
