from fastapi import APIRouter, HTTPException, Depends

from . import models
from app.datacenter.base_defs import DatacenterModelKey

router = APIRouter()


@router.get("/{id}", response_model=models.Item)
async def get_passivity(key: DatacenterModelKey = Depends()):
    """Gets passivity from datacenter database"""
    item = await models.Item.find_one(key.__dict__)

    if item is None:
        raise HTTPException(status_code=404, detail="Item was not found")

    return item
