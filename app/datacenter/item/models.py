from typing import List, Union, Optional

import app.datacenter.base_defs as base

from app.database.database import database, EmbeddedModel
from app.datacenter import enums
from app.datacenter.abnormality.models import Abnormality
from app.datacenter.gear.models import EquipmentData, Enchant
from app.datacenter.glyph.models import Glyph
from app.datacenter.passivity.models import PassivityCategory, Passivity
from app.types import Link


class Crystal(EmbeddedModel):
    destroy_probability: float
    takes_slots: int
    passivities: List[Union[Link[Passivity], Passivity]]


class SkillEffect(EmbeddedModel):
    hp_diff: Optional[float] = None
    mp_diff: Optional[float] = None
    abnormality: Optional[Union[Link[Abnormality], Abnormality]] = None


class Item(base.DatacenterModel):
    name: str = None
    tooltip: str = None
    icon: str = None

    rarity: enums.Rarity

    equipment: Optional[Union[Link[EquipmentData], EquipmentData]] = None
    level: int = None
    required_classes: List[enums.Klass] = []

    bind_type: enums.BindType

    tradable: bool
    store_sellable: bool
    warehouse_storable: bool
    guild_warehouse_storable: bool
    user_warehouse_storable: Optional[bool] = None

    sales_price: Optional[int] = None

    passivity_categories: List[Union[Link[PassivityCategory], PassivityCategory]] = []
    passivities: List[Union[Link[Passivity], Passivity]] = []

    max_enchant: Optional[int] = None
    enchant: Optional[Union[Link[Enchant], Enchant]] = None

    crystal: Optional[Crystal] = None
    skill_effect: Optional[SkillEffect] = None
    glyph: Optional[Union[Link[Glyph], Glyph]] = None

    class Database(base.DatacenterModel.Database):
        collection = database.datacenter.item


async def init() -> None:
    await Item.init()
