from typing import List, Optional

from pydantic import Schema

from app.datacenter.abnormality.models import Abnormality
from app.datacenter.gear.models import EquipmentData, Enchant
from app.datacenter.glyph.models import Glyph
from app.datacenter.passivity.models import Passivity, PassivityCategory
from app.types import DcUploadLink
from . import models


class Crystal(models.Crystal):
    passivities: List[DcUploadLink[Passivity]]


class SkillEffect(models.SkillEffect):
    abnormality: DcUploadLink[Abnormality]


class Item(models.Item):
    equipment: DcUploadLink[EquipmentData] = None

    passivity_categories: List[DcUploadLink[PassivityCategory]] = []
    passivities: List[DcUploadLink[Passivity]] = []

    enchant: DcUploadLink[Enchant] = None

    crystal: Optional[Crystal] = None
    skill_effect: Optional[SkillEffect] = None

    glyph: DcUploadLink[Glyph] = None

    @property
    def converts_to_class(self):
        return models.Item
