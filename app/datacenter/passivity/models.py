from typing import List, Union, Optional

from pydantic import Schema

import app.datacenter.base_defs as base

from app.database.database import database, EmbeddedModel
from app.types import Link, StrictInt


class PassivityCondition(EmbeddedModel):
    id: Optional[int] = None
    value: Optional[Union[StrictInt, float, str]] = None
    category: str = None


class Passivity(base.DatacenterModel):
    """Represents Passivity - a 'static' effect on a character"""

    name: Optional[str] = Schema(None, description="Passivity (usually hidden) name")
    internal_name: Optional[str] = Schema(
        None,
        description="Passivity internal name - usually in Korean. I hope utf8 works.",
    )
    tooltip: Optional[str] = Schema(
        None, description="Passivity tooltip: Caution! Currently unrendered !"
    )
    generated_tooltip: Optional[str] = Schema(
        None, description="Generated tooltip according to the rules"
    )
    icon: Optional[str] = Schema(
        None,
        description="Passivity icon name: Caution! Currently name "
        "is being returned, likely to change to url and/or path in future",
    )

    is_hidden: bool = Schema(
        False, description="True if passivity is hidden and not normally shown"
    )

    kind: int = Schema(
        ...,
        description="Group of passivities - only one of same kind can be active at once.\n"
        "**TODO:** check if this is actually true, now I'm not sure...",
    )
    method: int = Schema(
        ...,
        description="Represents different behaviours of effects, for example multiplicative and addtive.\n"
        "Nothing that has a standard tho...",
    )
    type: int = Schema(
        ...,
        description="Type of an passivity effect"
        " - *usually* represents an logical group of effects.\n"
        "For example, type 4 is Endurance effects.\n"
        "**NOTE:** different from abnormality types",
    )
    tick_interval: int = Schema(
        ...,
        description="Tick interval in seconds of some passivity types, for example HP regen",
    )
    value: Union[StrictInt, float, str] = Schema(
        ...,
        description="Value of an passivity effect. This can be wild and can be float, int or string. "
        "Note: the type of value is guessed, and might not match reality. "
        "TODO: Arrays currently not supported",
    )
    probability: float = Schema(..., description="Probability of passivity activating")

    condition: PassivityCondition = None

    class Database(base.DatacenterModel.Database):
        collection = database.datacenter.passivity


class PassivityCategory(base.DatacenterModel):
    """Represents PassivityCategory - a list of passivities"""

    is_rollable: bool = Schema(
        True, description="True if user can choose passivity out of this category"
    )

    passivities: List[Union[Link[Passivity], Passivity]] = Schema(
        [], description="List whose elements are either db ids, or whole Passivity"
    )

    class Database(base.DatacenterModel.Database):
        collection = database.datacenter.passivity_category


async def init() -> None:
    await Passivity.init()
    await PassivityCategory.init()
