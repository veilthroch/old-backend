from fastapi import APIRouter, HTTPException, Depends

from . import models
from app.datacenter.base_defs import DatacenterModelKey

router = APIRouter()


@router.get("/{id}", response_model=models.Passivity)
async def get_passivity(key: DatacenterModelKey = Depends()):
    """Gets passivity from datacenter database"""
    passivity = await models.Passivity.find_one(key.__dict__)

    if passivity is None:
        raise HTTPException(status_code=404, detail="Abnormality was not found")

    return passivity


@router.get("/category/{id}", response_model=models.PassivityCategory)
async def get_passivity_category(key: DatacenterModelKey = Depends()):
    """Gets passivity category from datacenter database"""
    category = await models.PassivityCategory.find_one(key.__dict__)
    print(category)
    await category.save()

    if category is None:
        raise HTTPException(status_code=404, detail="Abnormality was not found")

    return category
