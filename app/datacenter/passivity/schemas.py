from typing import List

from pydantic import Schema

from app.datacenter.passivity.models import Passivity
from app.types import DcUploadLink
from . import models


class PassivityCategory(models.PassivityCategory):
    """Represents PassivityCategory - a list of passivities"""

    passivities: List[DcUploadLink[Passivity]] = Schema(
        [], description="Ids of passivities this links to"
    )

    @property
    def converts_to_class(self):
        return models.PassivityCategory
