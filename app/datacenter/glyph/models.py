from typing import Union, Optional

from pydantic import Schema

import app.datacenter.base_defs as base

from app.database.database import database
from app.datacenter.passivity.models import Passivity
from app.types import Link


class Glyph(base.DatacenterModel):
    name: str = Schema(...)
    tooltip: str = Schema(...)
    skill_name: str = Schema(...)

    klass: str = Schema(..., alias="class")
    level: Optional[int] = Schema(None)
    rarity: int = Schema(...)
    cost: Optional[int] = Schema(None)

    parent: Optional["Glyph"] = Schema(None)
    passivity: Union[Link[Passivity], Passivity] = Schema(...)
    recommendation_priority: Optional[int] = Schema(None)

    class Database(base.DatacenterModel.Database):
        collection = database.datacenter.glyph


async def init() -> None:
    await Glyph.init()
