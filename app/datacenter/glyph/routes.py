from fastapi import APIRouter, HTTPException, Depends

from . import models
from app.datacenter.base_defs import DatacenterModelKey

router = APIRouter()


@router.get("/{id}", response_model=models.Glyph)
async def get_passivity(key: DatacenterModelKey = Depends()):
    """Gets passivity from datacenter database"""
    glyph = await models.Glyph.find_one(key.__dict__)

    if glyph is None:
        raise HTTPException(status_code=404, detail="Abnormality was not found")

    return glyph
