from typing import List, Optional

from pydantic import Schema

import app.datacenter.glyph.models as glyph_models
from app.datacenter.passivity.models import Passivity
from app.types import DcUploadLink
from . import models


class Glyph(models.Glyph):
    passivity: DcUploadLink[Passivity] = Schema(
        ..., description="Ids of passivities this links to"
    )
    parent: Optional[DcUploadLink[models.Glyph]] = Schema(
        None, description="Ids of parent glyphs this links to"
    )

    @property
    def converts_to_class(self):
        return models.Glyph
