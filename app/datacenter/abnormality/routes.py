from fastapi import APIRouter, HTTPException, Depends

from app.datacenter import enums
from app.datacenter.abnormality import models
from app.datacenter.base_defs import DatacenterModelKey

router = APIRouter()


@router.get("/{id}", response_model=models.Abnormality)
async def get_abnormality(key: DatacenterModelKey = Depends()):
    """Gets abnormality from datacenter database"""
    abnormal = await models.Abnormality.find_one(key.__dict__)

    if abnormal is None:
        raise HTTPException(status_code=404, detail="Abnormality was not found")

    return abnormal
