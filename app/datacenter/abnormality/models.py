from typing import List, Union

from pydantic import Schema

import app.datacenter.base_defs as base

from app.database.database import database, EmbeddedModel
from app.types import StrictInt


class AbnormalityKind(EmbeddedModel):
    """Represents a exclusive group of abormalities"""

    id: int = Schema(..., description="Identification of the Kind")
    name: str = Schema(
        None,
        description="Name of group - shown in abnormality tooltip. Caution! Currently unparsed !",
    )


class AbnormalityEffect(EmbeddedModel):
    """
    Represents an effect of abnormality - for example 'increase crit factor by 5'\n
    Visual attributes of effects are currently unmapped
    """

    method: int = Schema(
        ...,
        description="Represents different behaviours of effects, for example multiplicative and addtive.\n"
        "Nothing that has a standard tho...",
    )
    type: int = Schema(
        ...,
        description="Type of an effect"
        " - *usually* represents an logical group of effects.\n"
        "For example, type 4 is Endurance effects",
    )
    tick_interval: int = Schema(
        ...,
        description="Tick interval in seconds of some effect types, for example HP regen",
    )
    value: Union[StrictInt, float, str] = Schema(
        ...,
        description="Value of an effect. This can be wild and can be float, int or string. "
        "Note: the type of value is guessed, and might not match reality. "
        "TODO: Arrays currently not supported",
    )


class Abnormality(base.DatacenterModel):
    """Represents Abnormality - a buff or debuff"""

    name: str = Schema(None, description="Abnormality name as shown in UI")
    tooltip: str = Schema(
        None, description="Abnormality tooltip: Caution! Currently unrendered !"
    )
    icon: str = Schema(
        None,
        description="Abnormality icon name: Caution! Currently name "
        "is being returned, likely to change to url and/or path in future",
    )

    duration: int = Schema(
        ..., description="Duration of abnormality in seconds. Might be large"
    )
    kind: AbnormalityKind = Schema(
        None,
        description="Group of abnormalities - only one of same kind can be active at once",
    )
    level: int = Schema(
        ...,
        description="'Level' of abnormality "
        "- in case of kind-conflict, new abnormality is only applied "
        "if of same or higher level than already existing abnormality",
    )
    priority: int = Schema(..., description="Dunno ? (actually I know, but it's memes)")

    is_infinite: bool = Schema(
        ..., description="True if abnormality is infinite - without duration"
    )
    is_buff: bool = Schema(
        ...,
        description="True if abnormality is beneficial effect and is shown in upper row of the abnormal UI",
    )
    is_hidden: bool = Schema(
        ..., description="True if abnormality is hidden and not normally shown"
    )

    effects: List[AbnormalityEffect] = Schema(
        [],
        description="List of abnormality effects - each describes what abnormality does",
    )

    class Database(base.DatacenterModel.Database):
        collection = database.datacenter.abnormality


async def init() -> None:
    await Abnormality.init()
