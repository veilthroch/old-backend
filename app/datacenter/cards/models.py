from __future__ import annotations

from typing import List, Optional

import app.datacenter.base_defs as base

from app.database.database import database, EmbeddedModel
from app.datacenter.passivity.models import Passivity
from app.types import DcForeignKey


class Card(base.DatacenterModel):
    category: int
    level: int
    property: int
    effect: Optional[str] = None
    items_needed: int
    cost: int
    image: str
    superior_card: Optional[DcForeignKey[Card]] = None
    passivities: List[DcForeignKey[Passivity]]
    exp: int
    name: str
    source: str
    lore: str

    class Database:
        collection = database.datacenter.card


Card.update_forward_refs()


class CardCombineEffect(EmbeddedModel):
    passivity: DcForeignKey[Passivity]
    all_card_min_level: int = 0


class CardCombine(base.DatacenterModel):
    name: str
    categories: List[int] = []
    effects: List[CardCombineEffect]

    class Database:
        collection = database.datacenter.card_combine


async def init() -> None:
    await CardCombine.init()
    await Card.init()
