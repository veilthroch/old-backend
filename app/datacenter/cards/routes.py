from typing import List

from fastapi import APIRouter, HTTPException, Depends

from app.database.resolve import DbResolver
from . import models
from app.datacenter.base_defs import DatacenterKey, DatacenterModelKey

router = APIRouter()


@router.get("/all", response_model=List[models.Card])
async def get_all_cards(key: DatacenterKey = Depends()):
    """Gets all cards from datacenter database"""
    cards = await models.Card.find(key.__dict__)

    res = DbResolver(cards)
    await res.resolve()

    return res.data


@router.get("/combines", response_model=List[models.CardCombine])
async def get_all_cards(key: DatacenterKey = Depends()):
    """Gets all cards from datacenter database"""
    res = await models.CardCombine.find(key.__dict__)

    res = DbResolver(res)
    await res.resolve()

    return res.data


@router.get("/{id}", response_model=models.Card)
async def get_all_cards(key: DatacenterModelKey = Depends()):
    """Gets all cards from datacenter database"""
    card = await models.Card.find(key.__dict__)

    res = DbResolver(card)
    await res.resolve()

    return res.data[0]
