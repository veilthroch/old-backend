from typing import List, Optional

from pydantic import Schema

from app.types import DcUploadLink
from . import models
