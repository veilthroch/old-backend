import asyncio
import time
from collections import defaultdict
from typing import List, Dict, Tuple, Type

from fastapi import APIRouter, Depends
from pydantic import Schema

from app.datacenter import passivity, cards
from app.datacenter.base_defs import DatacenterKey
from app.types import BaseDcLink
from . import abnormality
from . import models
from app.utils import workers, camel_case
import app.datacenter.passivity.schemas as passivity_schemas
import app.datacenter.glyph.schemas as glyph_schemas
import app.datacenter.gear.schemas as gear_schemas

# so it doesn't need to repeat all the params
internal_router = APIRouter()
internal_router.include_router(abnormality.router, prefix="/abnormality")
internal_router.include_router(passivity.router, prefix="/passivity")
internal_router.include_router(cards.router, prefix="/card")


router = APIRouter()
router.include_router(
    internal_router,
    prefix="/{region}/patch/{patch}",
    dependencies=[Depends(DatacenterKey)],
)


async def upload_worker(
    queue: asyncio.Queue, traverser: "UploadTraverser", times: Dict[str, float]
) -> None:
    while True:
        started = time.time()

        name, id = await queue.get()

        for model in traverser.save(name, id):
            await model.save()

        queue.task_done()
        times[name] += time.time() - started


WORKERS = 1


class Empty:
    pass


class DatacenterUpload(camel_case.CamelCaseSchema):
    # items: List[item_schemas.Item] = Schema(...)
    cards: List[models.Card] = []
    card_combines: List[models.CardCombine] = []

    enchant_data: List[gear_schemas.Enchant] = Schema([])
    equipment_data: List[models.EquipmentData] = Schema([])
    glyphs: List[glyph_schemas.Glyph] = Schema(
        [], description="List of parsed glyph objects"
    )
    abnormalities: List[models.Abnormality] = Schema(
        [], description="List of parsed abnormality objects"
    )
    passivities: List[models.Passivity] = Schema(
        [], description="List of parsed passivity objects"
    )
    passivity_categories: List[passivity_schemas.PassivityCategory] = Schema(
        [], description="List of parsed passivity categories"
    )


class UploadTraverser:
    _stored_ids: Dict[int, int]

    def __init__(self, upload):
        self.upload = upload
        self._load_mapping()

    def _load_mapping(self):
        self.mapping = {}

        for field_name in self.upload.fields.keys():
            for model in getattr(self.upload, field_name):
                self.mapping[self.get_key(model)] = model

    def get_unsaved_dependencies(self, name, id, include_this=False):
        model = self.mapping.get((name, id), Empty())

        if isinstance(model, Empty):
            print(f"Warning: {name}/{id} refered to, but not found !")
            model.pk = "000000000000000000000000"
            self.mapping[name, id] = model
            return

        data = self.mapping[name, id].dict()

        for link in self.find_links(data):
            target_key = (link.target_name(), link.id)
            yield from self.get_unsaved_dependencies(*target_key, include_this=True)

        if include_this and not self.mapping[name, id].pk:
            yield name, id

    def has_unsaved_dependencies(self, field, id):
        if self.mapping[field, id].pk:
            return False
        try:
            next(self.get_unsaved_dependencies(field, id))
            return True
        except StopIteration:
            return False

    def save(self, name, id):
        """
        Converts model into type for database
        Returns iterable of db models that need saving
        """

        model_key = (name, id)
        model = self.mapping[model_key]

        if model.pk:
            # Model is saved in db, no work needed
            return

        data = model.dict()

        for dependency in self.get_unsaved_dependencies(name, id):
            yield from self.save(*dependency)

        data = self.resolve_links(data)
        res = self.get_target_cls(model)(**data)
        yield res
        model.pk = res.pk

    @staticmethod
    def get_target_cls(model) -> Type:
        return (
            model.converts_to_class
            if hasattr(model, "converts_to_class")
            else model.__class__
        )

    @classmethod
    def get_key(cls, model) -> Tuple[str, int]:
        return cls.get_target_cls(model).__name__, model.id

    def models(self, verbose=False):
        """
        Yields Models for saving to db in correct order
        Assumes non-cyclic relationships!
        """
        for field_name in self.upload.fields.keys():
            start = time.time()
            if verbose:
                print(" -  ", field_name, "... ", end="")

            for model in getattr(self.upload, field_name):
                yield from self.save(*self.get_key(model))

            if verbose:
                print(time.time() - start)

    async def enqueue_keys(self, queue):
        for field_name in self.upload.fields.keys():
            for model in getattr(self.upload, field_name):
                await queue.put(self.get_key(model))

    @classmethod
    def find_links(cls, obj):
        if isinstance(obj, BaseDcLink):
            yield obj
        elif isinstance(obj, list):
            for item in obj:
                yield from cls.find_links(item)
        elif isinstance(obj, dict):
            for key, value in obj.items():
                yield from cls.find_links(value)

    def resolve_links(self, obj):
        if isinstance(obj, BaseDcLink):
            target = self.mapping[obj.target_name(), obj.id]

            if not target.pk:
                raise ValueError(f"{repr(obj)} wasn't saved properly !")

            if target.pk == "000000000000000000000000":
                return None

            return target.pk

        elif isinstance(obj, list):
            return [self.resolve_links(item) for item in obj]
        elif isinstance(obj, dict):
            return {key: self.resolve_links(value) for key, value in obj.items()}
        else:
            return obj


@router.post("/upload")
async def upload_datacenter(upload: DatacenterUpload):
    """Upload of datacenter. Maybe temporary ?"""

    print("Validation passed ! Storing:")
    started = time.time()

    traverser = UploadTraverser(upload)

    if WORKERS <= 1:
        for model in traverser.models(verbose=True):
            await model.save()
    else:
        times = defaultdict(float)

        queue, tasks = workers.create(WORKERS, upload_worker, args=(traverser, times))
        await traverser.enqueue_keys(queue)

        await queue.join()
        await workers.stop(tasks)

        for key, took in times.items():
            print(f" -  {key}: {took/WORKERS}")

    print("Total:", time.time() - started)
    return True
