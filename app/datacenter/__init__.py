from app.datacenter import gear, glyph
from . import abnormality
from . import passivity
from .routes import router


async def init() -> None:
    await abnormality.init()
    await passivity.init()
    await gear.init()
    await glyph.init()
