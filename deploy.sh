mkdir /srv/nginx/pages/$ENV_NAME/
sudo rsync -av --delete . /srv/nginx/pages/$ENV_NAME/api/
cd /srv/nginx/pages/$ENV_NAME/api/ || exit

python3.7 -m venv venv
source venv/bin/activate
pip install -r requirements/prod.txt
circusctl --endpoint ipc:///var/circus/endpoint-$ENV_NAME quit
circusd --daemon circus.ini