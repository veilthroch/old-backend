import asyncio
import os.path

from uvicorn import Config, Server

from app import app

if __name__ == "__main__":
    config = Config(app=app)
    server = Server(config)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(server.serve())
